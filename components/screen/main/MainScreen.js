import * as React from 'react';
import { Text, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

function Rooms() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Rooms Available!</Text>
    </View>
  );
}

function PostRoom() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Post Room!</Text>
    </View>
  );
}

function Profile() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>My Profile!</Text>
    </View>
  );
}

const Tab = createBottomTabNavigator();

export default function MainScreen() {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Rooms"
        component={Rooms}
        options={{ tabBarIcon: () => (<Ionicons name={'home-outline'} size={20} />) }} />

      <Tab.Screen name="Post Room"
        component={PostRoom}
        options={{ tabBarIcon: () => (<Ionicons name={'newspaper-outline'} size={20} />) }} />

      <Tab.Screen name="Profile"
        component={Profile}
        options={{ tabBarIcon: () => (<Ionicons name={'person-circle-outline'} size={20} />) }} />
    </Tab.Navigator>
  );
}