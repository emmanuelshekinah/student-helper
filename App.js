import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import SignInScreen from './components/screen/auth/SignInScreen';
import SignUpScreen from './components/screen/auth/SignUpScreen';
import MainScreen from './components/screen/main/MainScreen';

const Stack = createNativeStackNavigator();

const App=() => {
console.info("App")
  return (
    <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen name="SignIn" component={SignInScreen}/>
      <Stack.Screen name="SignUp" component={SignUpScreen} />
      <Stack.Screen name="Main" 
      options={{
        title: 'Student Accommodation',
      }}      
      component={MainScreen} />
    </Stack.Navigator>
  </NavigationContainer>
   
  );
};


export default App;
